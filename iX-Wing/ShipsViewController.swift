//
//  FirstViewController.swift
//  iX-Wing
//
//  Created by Flori on 10/03/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import UIKit

class ShipsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var shipsCollection: UICollectionView!
    
    let rebelShips = [
        Ship(name: "X-Wing", factionID: 1, stats: [3, 2, 3, 2], canonicalName: "xwing", actions: ["focus", "targetlock"], pilots: ["9 •Wedge Antilles", "8 •Luke Skywalker", "8 •Wes Janson", "7 •Jek Porkins", "6 •Garven Dreis", "5 •Biggs Darklighter", "5 •\"Hobbie\" Klivian", "4 Red Squadron Pilot", "3 •Tarn Mison", "2 Rookie Pilot"]),
        Ship(name: "Y-Wing", factionID: 1, stats: [2, 1, 5, 3], canonicalName: "ywing", actions: ["focus", "targetlock"], pilots: ["8 •Horton Salm", "6 •\"Dutch\" Vander", "4  Gray Squadron Pilot", "2  Gold Squadron Pilot"]),
        Ship(name: "A-Wing", factionID: 1, stats: [2, 3, 2, 2], canonicalName: "awing", actions: ["focus", "targetlock", "boost", "evade"], pilots: ["8 •Tycho Celchu", "7 •Jake Farrell", "6 •Arvel Crynyd", "5 •Gemmer Sojan", "3 Green Squadron Pilot", "1 Prototype Pilot"]),
        Ship(name: "YT-1300", factionID: 1, stats: [3, 1, 8, 5], canonicalName: "yt1300", actions: ["focus", "targetlock"], pilots: ["9 •Han Solo (HotR)", "9 •Han Solo", "8 •Rey", "7 •Lando Calrissian","5 •Chewbacca (HotR)" , "5 •Chewbacca", "3 Resistance Sympathizer", "1 Outer Rim Smuggler"]),
        Ship(name: "B-Wing", factionID: 1, stats: [3, 1, 5, 3], canonicalName: "bwing", actions: ["focus", "targetlock", "barrelroll"], pilots: ["8 •Ten Numb", "7 •Keyan Farlander", "6 •Ibtisam", "5 •Nera Dantels", "4 Dagger Squadron Pilot", "2 Blue Squadron Pilot"]),
        Ship(name: "HWK-290", factionID: 1, stats: [1, 2, 4, 1], canonicalName: "hwk290", actions: ["focus", "targetlock"], pilots: ["8 •Jan Ors", "6 •Kyle Katarn", "4 •Roark Garnett", "2 Rebel Operative"]),
        Ship(name: "E-Wing", factionID: 1, stats: [3, 3, 2, 3], canonicalName: "ewing", actions: ["focus", "targetlock", "barrelroll", "evade"], pilots: ["8 •Corran Horn", "5 •Etahn A'baht", "3 Blackmoon Squadron Pilot", "1 Knave Squadron Pilot"]),
        Ship(name: "Z-95 Headhunter", factionID: 1, stats: [2, 2, 2, 2], canonicalName: "z95headhunter", actions: ["focus", "targetlock"], pilots: ["8 •Airen Cracken", "6 •Lieutenant Blount", "4 Tala Squadron Pilot", "2 Bandit Squadron Pilot"]),
        Ship(name: "YT-2400", factionID: 1, stats: [2, 2, 5, 5], canonicalName: "yt2400", actions: ["focus", "targetlock", "barrelroll"], pilots: ["7 •Dash Rendar", "5 •\"Leebo\"", "3 •Eaden Vrill", "2 Wild Space Fringer"]),
        Ship(name: "K-Wing", factionID: 1, stats: [2, 1, 5, 4], canonicalName: "kwing", actions: ["focus", "targetlock", "slam"], pilots: ["8 •Miranda Doni", "6 •Esege Tuketu", "4 Guardian Squadron Pilot", "2 Warden Squadron Pilot"]),
        Ship(name: "T-70 X-Wing", factionID: 1, stats: [3, 2, 3, 3], canonicalName: "t70xwing", actions: ["focus", "targetlock", "boost"], pilots: ["9 •Poe Dameron (HotR)", "8 •Poe Dameron", "7 •Ello Asty", "7 •Nien Numb", "6 •\"Red Ace\"", "6 •\"Snap\" Wexley", "5 •\"Blue Ace\"", "4 Red Squadron Veteran", "3 •Jess Pava", "2 Blue Squadron Novice"]),
        Ship(name: "VCX-100", factionID: 1, stats: [4, 0, 10, 6], canonicalName: "vcx100", actions: ["focus", "targetlock", "evade"], pilots: ["7 •Hera Syndulla", "5 •Kanan Jarrus", "4 •\"Chopper\"", "3 Lothal Rebel"]),
        Ship(name: "Attack Shuttle", factionID: 1, stats: [3, 2, 2, 2], canonicalName: "attackshuttle", actions: ["focus", "barrelroll", "evade"], pilots: ["7 •Hera Syndulla", "5 •Sabine Wren", "4 •Ezra Bridger", "3 •\"Zeb\" Orrelios"]),
        Ship(name: "ARC-170", factionID: 1, stats: [2,1,6,3], canonicalName: "arc170", actions: ["focus", "targetlock"], pilots: ["7 •Norra Wexley", "6 •Shara Bey", "4 •Thane Kyrell", "3 •Braylen Stramm"]),
        Ship(name: "U-Wing", factionID: 1, stats: [3, 1, 4, 4], canonicalName: "uwing", actions: ["focus", "targetlock"], pilots: ["6 •Cassian Andor", "4 •Bodhi Rook", "3 •Heff Tobber", "2 Blue Squadron Pathfinder"]),
        Ship(name: "Sabine's TIE Fighter", factionID: 1, stats: [2, 3, 3, 0], canonicalName: "sabinestiefighter", actions: ["focus", "barrelroll", "evade"], pilots: ["7 •Ahsoka Tano", "5 •Sabine Wren", "4 •Captain Rex", "3 •\"Zeb\" Orrelios"]),
        Ship(name: "Auzituck Gunship", factionID: 1, stats: [3,1,6,3], canonicalName: "auzituckgunship", actions: ["focus", "reinforce"], pilots: ["7 •Wullffwarro", "5 •Lowhhrick", "3 Wookie Liberator", "1 Kashyyyk Defender"]),
        Ship(name: "Scurrg H-6 Bomber", factionID: 1, stats: [3,1,5,5], canonicalName: "scurrgh6bomberR", actions: ["focus", "targetlock", "barrelroll"], pilots: ["8 •Captain Nym"]),
        Ship(name: "Sheathipede-Class Shuttle", factionID: 1, stats: [2,2,4,1], canonicalName: "sheathipedeclassshuttle", actions: ["focus", "targetlock", "coordinate"], pilots: ["9 •Fenn Rau", "5 •Ezra Bridger", "3 •\"Zeb\" Orrelios", "1 •AP-5"]),
        Ship(name: "B/SF-17 Bomber", factionID: 1, stats: [2,1,9,3], canonicalName: "bsf17bomber", actions: ["focus", "targetlock"], pilots: ["7 •\"Crimson Leader\"", "6 •\"Cobalt Leader\"", "4 •\"Crimson Specialist\"", "1 Crimson Squadron Pilot"])
    ]
    
    let imperialShips = [
        Ship(name: "TIE Fighter", factionID: 2, stats: [2, 3, 3, 0], canonicalName: "tiefighter", actions: ["focus", "barrelroll", "evade"], pilots: ["8 •\"Howlrunner\"", "7 •\"Mauler Mithel\"", "7 •\"Scourge\"", "6 •\"Dark Curse\"", "6 •\"Backstabber\"", "6 •\"Youngster\"", "5 •\"Winged Gundark\"", "5 •\"Night Beast\"", "4 •\"Wampa\"", "4 Black Squadron Pilot", "3 •\"Chaser\"", "3 Obsidian Squadron Pilot", "1 Acadamy Pilot"]),
        Ship(name: "TIE advanced", factionID: 2, stats: [2, 3, 3, 2], canonicalName: "tieadvanced", actions: ["focus", "targetlock", "barrelroll", "evade"], pilots: ["9 •Darth Vader", "8 •Juno Eclipse", "7 •Maarek Stele", "6 •Zertik Strom", "5 •Commander Alozen", "4 Storm Squadron Pilot", "3 •Lieutenant Colzet", "2 Tempest Squadron Pilot"]),
        Ship(name: "TIE Interceptor", factionID: 2, stats: [3, 3, 3, 0], canonicalName: "tieinterceptor", actions: ["focus", "barrelroll", "boost", "evade"], pilots: ["9 •Soontir Fel", "8 •Carnor Jax", "7 •Turr Phennir", "7 •Tetran Cowall", "6 •Kir Kanos", "6 Royal Guard Pilot", "5 •\"Fel's Wrath\"", "5 •Lieutenant Lorrir", "4 Saber Squadron Pilot", "3 Avenger Squadron Pilot", "1 Alpha Squadron Pilot"]),
        Ship(name: "Firespray-31", factionID: 2, stats: [3, 2, 6, 4], canonicalName: "firespray31", actions: ["focus", "targetlock", "evade"], pilots: ["8 •Boba Fett", "7 •Kath Scarlet", "5 •Krassis Trelix", "3 Bounty Hunter"]),
        Ship(name: "TIE Bomber", factionID: 2, stats: [2, 2, 6, 0], canonicalName: "tiebomber", actions: ["focus", "targetlock", "barrelroll"], pilots: ["8 •Tomax Bren", "7 •Major Rhymer", "6 •Captain Jonus", "5 Gamma Squadron Veteran", "4 Gamma Squadron Pilot", "3 •\"Deathfire\"", "2 Scimitar Squadron Pilot"]),
        Ship(name: "Lambda-Class Shuttle", factionID: 2, stats: [3, 1, 5, 5], canonicalName: "lambdaclassshuttle", actions: ["focus", "targetlock"], pilots: ["8 •Captain Kagi", "6 •Colonel Jendon", "4 •Captain Yorr", "2 Omicron Group Pilot"]),
        Ship(name: "TIE Defender", factionID: 2, stats: [3, 3, 3, 3], canonicalName: "tiedefender", actions: ["focus", "targetlock", "barrelroll"], pilots: ["8 •Rexler Brath", "7 •Maarek Stele", "6 •Colonel Vessery", "6 Glaive Squadron Pilot", "5 •Countess Ryad", "3 Onyx Squadron Pilot", "1 Delta Squadron Pilot"]),
        Ship(name: "TIE Phantom", factionID: 2, stats: [4, 2, 2, 2], canonicalName: "tiephantom", actions: ["focus", "barrelroll", "evade", "cloak"], pilots: ["7 •\"Whisper\"", "6 •\"Echo\"", "5 Shadow Squadron Pilot", "3 Sigma Squadron Pilot"]),
        Ship(name: "VT-49 Decimator", factionID: 2, stats: [3, 0, 12, 4], canonicalName: "vt49decimator", actions: ["focus", "targetlock"], pilots: ["8 •Rear Admiral Chiraneau", "6 •Commander Kenkirk", "4 •Captain Oicunn", "3 Patrol Leader"]),
        Ship(name: "TIE Punisher", factionID: 2, stats: [3, 1, 8, 5], canonicalName: "tiepunisher", actions: ["focus", "targetlock", "boost"], pilots: ["7 •\"Redline\"", "6 •\"Deathrain\"", "4 Black Eight Squadron Pilot", "2 Cutlass Squadron Pilot"]),
        Ship(name: "TIE/fo Fighter", factionID: 2, stats: [2, 3, 3, 1], canonicalName: "tiefofighter", actions: ["focus", "targetlock", "barrelroll", "evade"], pilots: ["8 •\"Omega Leader\"", "7 •\"Omega Ace\"", "7 •\"Zeta Leader\"", "6 •\"Epsilon Leader\"", "5 •\"Zeta Ace\"", "4 •\"Epsilon Ace\"", "4 Omega Squadron Pilot", "3 Zeta Squadron Pilot", "1 Epsilon Squadron Pilot"]),
        Ship(name: "TIE Advanced Prototype", factionID: 2, stats: [2, 3, 2, 2], canonicalName: "tieadvprototype", actions: ["focus", "targetlock", "barrelroll", "boost"], pilots: ["8 •The Inquisitor", "6 •Valen Rudor", "4 Baron of the Empire", "2 Sienar Test Pilot"]),
        Ship(name: "TIE/sf Fighter", factionID: 2, stats: [2,2,3,3], canonicalName: "tiesffighter", actions: ["focus", "targetlock", "barrelroll"], pilots: ["9 •\"Quickdraw\"", "7 •\"Backdraft\"", "5 Omega Specialist", "3 Zeta Specialist"]),
        Ship(name: "TIE Striker", factionID: 2, stats: [3, 2, 4, 0], canonicalName: "tiestriker", actions: ["focus", "barrelroll", "evade"], pilots: ["8 •\"Duchess\"", "6 •\"Pure Sabacc\"", "5 •\"Countdown\"", "4 Black Squadron Scout", "3 Scarif Defender", "1 Imperial Trainee"]),
        Ship(name: "Upsilon Class Shuttle", factionID: 2, stats: [4, 1, 6, 6], canonicalName: "upsilonclassshuttle", actions: ["focus", "targetlock", "coordinate"], pilots: ["6 •Kylo Ren", "4 •Major Stridan", "3 •Lieutenant Dormitz", "2 Starkiller Base Pilot"]),
        Ship(name: "TIE Aggressor", factionID: 2, stats: [2,2,4,1], canonicalName: "tieaggressor", actions: ["focus", "targetlock", "barrelroll"], pilots: ["7 •Lieutenant Kestal", "5 Onyx Squadron Escort", "4 •\"Double Edge\"", "2 Sienar Specialist"]),
        Ship(name: "Alpha-Class Star Wing", factionID: 2, stats: [2,2,4,3], canonicalName: "alphaclassstarwing", actions: ["focus", "targetlock", "slam", "reload"], pilots: ["7 •Major Vynder", "5 •Lieutenant Karsabi", "4 •Rho Squadron Veteran", "2 Nu Squadron Pilot"]),
        Ship(name: "TIE Silencer", factionID: 2, stats: [3,3,4,2], canonicalName: "tiesilencer", actions: ["focus", "targetlock", "barrellroll", "boost"], pilots: ["9 •Kylo Ren", "7 •Test Pilot \"Blackout\"", "6 First Order Test Pilot", "4 Sienar-Jaemus Analyst"])
    ]
    
    let scumShips = [
        Ship(name: "Y-Wing", factionID: 3, stats: [2, 1, 5, 3], canonicalName: "ywingS", actions: ["focus", "targetlock"], pilots: ["7 •Kavil", "5 •Drea Renthal", "4 Hired Gun", "2 Syndicate Thug"]),
        Ship(name: "Z-95 Headhunter", factionID: 3, stats: [2, 2, 2, 2], canonicalName: "z95headhunterS", actions: ["focus", "targetlock"], pilots: ["7 •N'Dru Suhlak", "5 •Kaa'to Leeachos", "3 Black Sun Soldier", "1 Binayre Pirate", "* •Nashtah Pup Pilot"]),
        Ship(name: "HWK-290", factionID: 3, stats: [1, 2, 4, 1], canonicalName: "hwk290S", actions: ["focus", "targetlock"], pilots: ["7 •Dace Bonearm", "5 •Palob Godalhi", "3 •Torkhil Mux", "1 Spice Runner"]),
        Ship(name: "Firespray-31", factionID: 3, stats: [3, 2, 6, 4], canonicalName: "firespray31S", actions: ["focus", "targetlock", "evade"], pilots: ["8 •Boba Fett", "7 •Kath Scarlet", "6 •Emon Azzameen", "5 Mandalorian Mercenary"]),
        Ship(name: "M3-A \"Scyk\" Interceptor", factionID: 3, stats: [2, 3, 2, 1], canonicalName: "m3ainterceptor", actions: ["focus", "targetlock", "barrelroll", "evade"], pilots: ["8 •Serissu", "7 •Genesis Red", "6 •Laetin A'shera", "6 •Quinn Jast", "5 Tansarii Point Veteran", "3 •Inaldra", "2 Cartel Spacer", "1 •Sunny Bounder"]),
        Ship(name: "StarViper", factionID: 3, stats: [3, 3, 4, 1], canonicalName: "starviper", actions: ["focus", "targetlock", "barrelroll", "boost"], pilots: ["7 •Prince Xizor", "6 •Dalan Oberos", "5 •Guri", "5 Black Sun Assassin", "4 •Thweek", "3 Black Sun Vigo", "1 Black Sun Enforcer"]),
        Ship(name: "IG-2000", factionID: 3, stats: [3, 3, 4, 4], canonicalName: "aggressor", actions: ["focus", "targetlock", "boost", "evade"], pilots: ["6 •IG88-A", "6 •IG88-B", "6 •IG88-C", "6 •IG88-D"]),
        Ship(name: "YV-666", factionID: 3, stats: [3, 1, 6, 6], canonicalName: "yv666", actions: ["focus", "targetlock"], pilots: ["7 •Bossk", "6 •Moralo Eval", "5 •Latts Razzi", "2 Trandoshan Slaver"]),
        Ship(name: "Kihraxz Fighter", factionID: 3, stats: [3, 2, 4, 1], canonicalName: "kihraxzfighter", actions: ["focus", "targetlock"], pilots: ["9 •Talonbane Cobra", "7 •Viktor Hel", "6 •Graz The Hunter", "5 Black Sun Ace", "4 •Captain Jostero", "2 Cartel Marauder"]),
        Ship(name: "JumpMaster 5000", factionID: 3, stats: [2, 2, 5, 4], canonicalName: "jumpmaster5000", actions: ["focus", "targetlock", "barrelroll"], pilots: ["9 •Dengar", "7 •Tel Trevura", "4 •Manaroo", "3 Contracted Scout"]),
        Ship(name: "G-1A Starfighter", factionID: 3, stats: [3, 1, 4, 4], canonicalName: "g1astarfighter", actions: ["focus", "targetlock", "evade"], pilots: ["7 •Zuckuss", "6 •4-LOM", "5 Gand Findsman", "3 Ruthless Freelancer"]),
        Ship(name: "Lancer-class Pursuit Craft", factionID: 3, stats: [3,2,7,3], canonicalName: "lancerclasspursuitcraft", actions: ["focus", "targetlock", "evade", "rotatearc"], pilots: ["7 •Ketsu Onyo", "6 •Asajj Ventress", "5 •Sabine Wren", "2 Shadowport Hunter"]),
        Ship(name: "Protectorate Starfighter", factionID: 3, stats: [3,3,4,0], canonicalName: "protectoratestarfighter", actions: ["focus", "targetlock", "barrelroll", "boost"], pilots: ["9 •Fenn Rau", "7 •Old Teroch", "6 •Kad Solus", "5 Concord Dawn Ace", "3 Concord Dawn Veteran", "1 Zealous Recruit"]),
        Ship(name: "Quadjumper", factionID: 3, stats: [2,2,5,0], canonicalName: "quadjumper", actions: ["focus", "barrelroll"], pilots: ["7 •Constable Zuvio", "5 •Sarco Plank", "3 •Unkar Plutt", "1 Jakku Gunrunner"]),
        Ship(name: "Scurrg H-6 Bomber", factionID: 3, stats: [3,1,5,5], canonicalName: "scurrgh6bomber", actions: ["focus", "targetlock", "barrelroll"], pilots: ["8 •Captain Nym", "6 •Sol Sixxa", "3 Lok Revenant", "1 Karthakk Pirate"]),
        Ship(name: "M12-L Kimogila Fighter", factionID: 3, stats: [3,1,6,2], canonicalName: "m12lkimogilafighter", actions: ["focus", "targetlock", "barrelroll", "reload"], pilots: ["8 •Torani Kulda", "7 •Dalan Oberos", "5 Cartel Executioner", "3 Cartel Brute"])
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        shipsCollection.delegate = self
        shipsCollection.dataSource = self
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shipCell", for: indexPath) as? ShipCollectionViewCell {
            let ship: Ship!
            if self.title == "Rebels" {
                ship = rebelShips[indexPath.row]
            } else if self.title == "Empire" {
                ship = imperialShips[indexPath.row]
            } else if self.title == "Scum" {
                ship = scumShips[indexPath.row]
            } else {
                ship = Ship(name: "Error", factionID: 0, stats: [0,0,0,0], canonicalName: "error", actions: [""], pilots: [""])
            }
            
            cell.configureCell(ship: ship)
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let shipToPass: Ship!
        
        if self.title == "Rebels" {
            shipToPass = rebelShips[indexPath.row]
            performSegue(withIdentifier: "rebelShipSegue", sender: shipToPass)
        } else if self.title == "Empire" {
            shipToPass = imperialShips[indexPath.row]
            performSegue(withIdentifier: "imperialShipSegue", sender: shipToPass)
        } else if self.title == "Scum" {
            shipToPass = scumShips[indexPath.row]
            performSegue(withIdentifier: "scumShipSegue", sender: shipToPass)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let shipToPass = sender as? Ship {
            if let navigationVC = segue.destination as? NavigationViewController {
                /*var shipToPass: Ship
                switch segue.identifier! as String {
                case "rebelShipSegue":
                    shipToPass = rebelShips[(buttonPressed as AnyObject).tag]
                    break
                case "imperialShipSegue":
                    shipToPass = imperialShips[(buttonPressed as AnyObject).tag]
                    break
                case "scumShipSegue":
                    shipToPass = scumShips[(buttonPressed as AnyObject).tag]
                    break
                default:
                    shipToPass = Ship(name: "Error", factionID: 0, stats: [0,0,0,0], canonicalName: "error", actions: [""], pilots: [""])
                }*/
                //let shipToPass = ships[(buttonPressed as AnyObject).tag]
                navigationVC.ship = shipToPass
                if let shipDetailsTVC = navigationVC.viewControllers.first as? ShipDetailsTableViewController {
                    shipDetailsTVC.ship = shipToPass
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.title == "Rebels" {
            return rebelShips.count
        } else if self.title == "Empire" {
            return imperialShips.count
        } else if self.title == "Scum" {
            return scumShips.count
        } else {
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

