//
//  Ship.swift
//  iX-Wing
//
//  Created by Flori on 12/03/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import Foundation

class Ship {
    fileprivate var _name: String
    fileprivate var _factionID: Int
    fileprivate var _stats: [Int]
    fileprivate var _canonicalName: String
    fileprivate var _actions: [String]
    fileprivate var _pilots: [String]
    
    var name: String {
        get {
            return _name
        }
    }
    
    var factionID: Int {
        get {
            return _factionID
        }
    }
    
    var stats: [Int] {
        get {
            return _stats
        }
    }
    
    var canonicalName: String {
        get {
            return _canonicalName
        }
    }
    
    var actions: [String] {
        get {
            return _actions
        }
    }
    
    var pilots: [String] {
        get {
            return _pilots
        }
    }
    
    init(name: String, factionID: Int, stats: [Int], canonicalName: String, actions: [String], pilots: [String]) {
        self._name = name
        self._factionID = factionID
        self._canonicalName = canonicalName
        self._stats = stats
        self._actions = actions
        self._pilots = pilots
    }
}


