//
//  ShipActionsCell.swift
//  iX-Wing
//
//  Created by Florian Poncelin on 17/08/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import UIKit

class ShipActionsCell: UITableViewCell {
    
    @IBOutlet weak var action0Img: UIImageView!
    @IBOutlet weak var action1Img: UIImageView!
    @IBOutlet weak var action2Img: UIImageView!
    @IBOutlet weak var action3Img: UIImageView!
    
    var ship: Ship!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(_ ship: Ship) {
        let imageViewArray = [action0Img, action1Img, action2Img, action3Img]
        for i in 0..<ship.actions.count {
            imageViewArray[i]?.image = UIImage(named: "\(ship.actions[i]).jpg")
        }
        if ship.actions.count < imageViewArray.count {
            for i in ship.actions.count..<imageViewArray.count {
                imageViewArray[i]?.isHidden = true
            }
        }
    }

}
