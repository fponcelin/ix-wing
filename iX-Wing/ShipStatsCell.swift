//
//  ShipStatsCell.swift
//  iX-Wing
//
//  Created by Florian Poncelin on 17/08/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import UIKit

class ShipStatsCell: UITableViewCell {
    
    @IBOutlet weak var attackLbl: UILabel!
    @IBOutlet weak var agilityLbl: UILabel!
    @IBOutlet weak var hullLbl: UILabel!
    @IBOutlet weak var shieldsLbl: UILabel!
    
    var ship: Ship!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(_ ship: Ship) {
        self.ship = ship
        attackLbl.text = "\(self.ship.stats[0])"
        agilityLbl.text = "\(self.ship.stats[1])"
        hullLbl.text = "\(self.ship.stats[2])"
        shieldsLbl.text = "\(self.ship.stats[3])"
    }

}
