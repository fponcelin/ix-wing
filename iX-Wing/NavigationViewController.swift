//
//  NavigationViewController.swift
//  iX-Wing
//
//  Created by Flori on 13/03/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {
    
    var ship: Ship?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let currentShip = ship {
            self.navigationBar.topItem?.title = currentShip.name
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let shipDetailTVC = segue.destination as? ShipDetailsTableViewController {
            if let shipToPass = ship {
                shipDetailTVC.ship = shipToPass
            }
        }
    }

}
