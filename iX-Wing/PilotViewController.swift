//
//  PilotViewController.swift
//  iX-Wing
//
//  Created by Flori on 13/03/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import UIKit

class PilotViewController: UIViewController {

    @IBOutlet weak var pilotImageView: UIImageView!
    var pilotImageName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let currentPilotImageName = pilotImageName {
            pilotImageView.image = UIImage(named: currentPilotImageName)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
