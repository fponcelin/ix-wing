//
//  ShipDetailsTableViewController.swift
//  iX-Wing
//
//  Created by Flori on 13/03/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import UIKit

extension UILabel {
    func kern(_ kerningValue:CGFloat) {
        self.attributedText =  NSAttributedString(string: self.text ?? "", attributes: [NSAttributedStringKey.kern:kerningValue, NSAttributedStringKey.font:self.font, NSAttributedStringKey.foregroundColor:self.textColor])
    }
}

class ShipDetailsTableViewController: UITableViewController {
    
    var ship: Ship!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let currentShip = ship {
            return 3 + currentShip.pilots.count
        } else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath as NSIndexPath).row {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "shipStats", for: indexPath) as? ShipStatsCell {
                cell.configureCell(ship)
                return cell
            } else {
                return ShipStatsCell()
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "shipActions", for: indexPath) as? ShipActionsCell {
                cell.configureCell(ship)
                return cell
            } else {
                return ShipActionsCell()
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "shipDial", for: indexPath) as? ShipDialCell {
                cell.configureCell(ship)
                return cell
            } else {
                return ShipDialCell()
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "shipPilots", for: indexPath)
            cell.textLabel?.text = ship.pilots[(indexPath as NSIndexPath).row - 3]
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath as NSIndexPath).row {
        case 0, 1:
            return 75
        case 2:
            return 250
        default:
            return 44
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let selectedPilot = sender as? UITableViewCell {
            let indexPath = tableView.indexPath(for: selectedPilot)!
            if let pilotVC = segue.destination as? PilotViewController {
                pilotVC.pilotImageName = "\(ship.canonicalName)-\(String((indexPath as NSIndexPath).row - 3)).jpg"
            }
        }
    }
    

    @IBAction func backBarButtonPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
