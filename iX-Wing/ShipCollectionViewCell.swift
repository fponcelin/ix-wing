//
//  ShipCollectionViewCell.swift
//  iX-Wing
//
//  Created by Florian Poncelin on 25/09/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import UIKit

class ShipCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var shipIcon: UIImageView!
    @IBOutlet weak var shipLbl: UILabel!
    
    var ship: Ship!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 5.0
    }
    
    func configureCell(ship: Ship) {
        self.ship = ship
        shipLbl.text = ship.name
        shipIcon.image = UIImage(named: "\(self.ship.canonicalName)")
    }
}
