//
//  ShipDialCell.swift
//  iX-Wing
//
//  Created by Florian Poncelin on 17/08/2016.
//  Copyright © 2016 Florian Poncelin. All rights reserved.
//

import UIKit

class ShipDialCell: UITableViewCell {
    
    @IBOutlet weak var dialImg: UIImageView!
    
    var ship: Ship!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(_ ship: Ship) {
        self.ship = ship
        dialImg.image = UIImage(named: "\(ship.canonicalName)-Move.png")
    }

}
